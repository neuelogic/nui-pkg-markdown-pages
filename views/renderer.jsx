import React from 'react';
import Remarkable from 'remarkable';
import RemarkableReactRenderer from 'remarkable-react';
import * as HighlightJS from 'highlight.js';

const slugify = (text) => String(text).replace(/\s+/ig, '-').replace(/[^A-Za-z0-9-]/, '').toLowerCase();

export default (nui) => class MarkdownRenderer extends nui.Component {
	componentWillMount() {
		const {
			source = false,
			path = ''
		} = this.props || {};

		this.initRemarkable();
		this.initRenderer();

		if (source) {
			this.fetchMarkdown({ source, path });
		}
	}

	componentWillReceiveProps({ source, path }) {
		const { props } = this;
		if (!source) {
			this.setState({ markdown: '' });
		} else if (source !== props.source || path !== props.path) {
			this.fetchMarkdown({ source, path });
		}
	}

	fetchMarkdown({ source, path = '' }) {
		if(typeof window !== 'undefined') {
			const fetchPromise = fetch(`${source}${path}.md`);

			fetchPromise.then(
				(res) => (res.status >= 200 && res.status < 300) ? res.text() : [res.status]
			).then( (text) => {
				if (text instanceof Array) {
					nui.Navigate('/' + (text[0] === 404 ? 'NotFound' : text[0]));
				} else this.setState({ markdown: text });
			});

			fetchPromise.catch( (e) => {
				nui.Navigate('/500');
			});
		}
	}

	initRemarkable() {
		this.headings = [];

		this.md = new Remarkable({
			highlight: (str, lang) => {
				if (lang && HighlightJS.getLanguage(lang)) {
					try {
						return HighlightJS.highlight(lang, str).value;
					} catch (err) {}
				}

				try {
					return HighlightJS.highlightAuto(str).value;
				} catch (err) {}

				return '';
			}
		}).use(remarkable => {
			remarkable.renderer.rules.heading_open = (tokens, idx) => {
				const heading = {
					level: tokens[idx].hLevel,
					title: tokens[idx + 1].content,
					slug: slugify(tokens[idx + 1].content)
				}

				this.headings.push(heading);

				return `<h${heading.level} id=${heading.slug}>`;
			};
		});
	}

	initRenderer() {
		const { props } = this;
		const propComponents = props.components || {};
		const {
			code = false,
			pre = false
		} = propComponents;

		const CodeComponent = code;
		const PreComponent = pre;

		const components = Object.assign({}, propComponents, {
			code: code ? ((props) => (
				<CodeComponent dangerouslySetInnerHTML={{__html: this.highlightChildren(props)}} />
			)) : ((props) => (
				<code dangerouslySetInnerHTML={{__html: this.highlightChildren(props)}} />
			)),
			pre: pre ? (({ children }) => (
				<PreComponent className="hljs">{children}</PreComponent>
			)) : (({ children }) => (
				<pre className="hljs">{children}</pre>
			))
		});

		this.md.renderer = new RemarkableReactRenderer({ components });
	}

	highlightChildren(props) {
		try {
			return HighlightJS.highlight('javascript', props.children).value;
		} catch (err) {}

		try {
			return HighlightJS.highlightAuto(props.children).value;
		} catch (err) {}

		return props.children;
	}

	extractGatedSection(id, source) {
 		try {
 			const startTag = `<!-- ${id} -->`;
 			const endTag = `<!-- end${id} -->`;

 			const collapsed = source.replace(/\n/g,' ')
 			const start = collapsed.indexOf(startTag);
 			const end = collapsed.indexOf(endTag);

 			if(start === -1 || end === -1) return ['', source];

 			const section = source.substring(start + startTag.length, end);
 			return [
 				section,
 				source.substr(0,start) + source.substr(end + endTag.length)
 			];
 		} catch(e) {}

 		return ['', source];
 	}

	render() {
		const { props, md } = this;

		const { markdown = false } = this.state;

		const {
			children,
			wrapper = React.DOM.div
		} = props;

		const Wrapper = wrapper;

		const content = markdown || children;

		let rendered;
		if (this.props.sections) {
			return this.renderSections(content, Wrapper);
		}

		return (
			<Wrapper className="markdown" sections={{}}>
				{md.render(content)}
			</Wrapper>
		);
	}

	renderSections(content, Wrapper) {
		let markdown = content;

		const { props, md } = this;
		const { sections } = props;

		const markdownSections = Object.keys(
			sections
		).reduce((components, sectionName) => {
			try {
				let sectionMD = '';

				[sectionMD, markdown] = this.extractGatedSection(
					String(sectionName).toLowerCase(), markdown
				);

				const Elem = sectionMD ? sections[sectionName] : (() => null);

				if (Elem === "raw") {
					components[sectionName] = md.render(sectionMD);
				} else {
					components[sectionName] = (
						<Elem
							className={sectionName}
							key={`markdown-section-${sectionName.toLowerCase()}`}
						>{md.render(sectionMD)}</Elem>
					);
				}
			} catch(e) {
				console.warn(e);
			}
			return components;
		}, {});

		return (
			<Wrapper className="markdown" sections={markdownSections}>
				{md.render(markdown)}
			</Wrapper>
		);
	}
};
